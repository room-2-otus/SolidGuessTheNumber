﻿using SolidGuessTheNumber.Interfaces;
using SolidGuessTheNumber.Models;

namespace SolidGuessTheNumber.Core
{
    /// <summary>
    /// Implement the core game logic
    /// </summary>
    public class GuessingGame : IGame
    {
        private readonly int _numberToGuess;
        private readonly GameSettings _settings;
        private int _triesLeft;

        public GuessingGame(GameSettings settings, IRandomNumberGenerator numberGenerator)
        {
            _settings = settings;
            _numberToGuess = numberGenerator.Generate(settings.MaxNumber);
            _triesLeft = settings.NumberOfTries;
        }

        public void Start()
        {
            Console.WriteLine($"Guess the number between 1 and {_settings.MaxNumber}. You have {_triesLeft} tries.");

            while (_triesLeft > 0)
            {
                Console.Write("Enter your guess: ");
                if (int.TryParse(Console.ReadLine(), out int guess))
                {
                    if (guess == _numberToGuess)
                    {
                        Console.WriteLine("Correct! You've won!");
                        return;
                    }

                    _triesLeft--;

                    if (_triesLeft > 0)
                    {
                        Console.WriteLine(guess < _numberToGuess ? "Higher!" : "Lower!");
                        Console.WriteLine($"You have {_triesLeft} tries left.");
                    }
                    else
                    {
                        Console.WriteLine("You've run out of tries. Game over!");
                        Console.WriteLine($"The correct number was {_numberToGuess}.");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input. Please enter a number.");
                }
            }
        }
    }
}
