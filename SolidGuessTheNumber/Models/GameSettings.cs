﻿namespace SolidGuessTheNumber.Models
{
    public class GameSettings
    {
        public int MaxNumber { get; set; }
        public int NumberOfTries { get; set; }
    }
}
