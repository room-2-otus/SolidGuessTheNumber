﻿using SolidGuessTheNumber.Interfaces;

namespace SolidGuessTheNumber.Services
{
    public class RandomNumberGenerator : IRandomNumberGenerator
    {
        private readonly Random _random = new Random();
        public int Generate(int maxNumber)
        {
            return _random.Next(1, maxNumber + 1); // +1 because upper bound is exclusive
        }
    }
}
