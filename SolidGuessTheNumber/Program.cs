﻿
using SolidGuessTheNumber.Core;
using SolidGuessTheNumber.Models;
using SolidGuessTheNumber.Services;

class Program
{
    static void Main()
    {
        var settings = new GameSettings
        {
            MaxNumber = 100,
            NumberOfTries = 10
        };

        var numberGenerator = new RandomNumberGenerator();
        var game = new GuessingGame(settings, numberGenerator);
        game.Start();
    }
}

