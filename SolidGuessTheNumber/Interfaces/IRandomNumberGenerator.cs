﻿namespace SolidGuessTheNumber.Interfaces
{
    public interface IRandomNumberGenerator
    {
        int Generate(int maxNumber);
    }
}
