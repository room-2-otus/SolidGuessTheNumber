# SOLID Principles Demonstration - Guess the Number Game

This `README` outlines the application of SOLID principles in the "Guess the Number" game. Each principle's application is described with specific examples from the code.

## Single Responsibility Principle (SRP)
- `GameSettings`: Holds configuration settings for the game. Its only responsibility is to maintain game settings data.
- `RandomNumberGenerator`: Responsible for generating random numbers within a given range.

## Open/Closed Principle (OCP)
- `IGame`: Defines a game's interface. New game types can be introduced by implementing this interface without modifying existing game logic.
- `GameSettings`: Can be extended to include additional settings without changing the existing behavior.

## Liskov Substitution Principle (LSP)
- `GuessingGame`: Derived from `IGame`, can replace `IGame` without affecting the game's operability, ensuring that derived classes can stand in for their base classes.

## Interface Segregation Principle (ISP)
- `IRandomNumberGenerator`: Used by `GuessingGame` for number generation without enforcing unnecessary methods on the game class.

## Dependency Inversion Principle (DIP)
- `GuessingGame` depends on the `IRandomNumberGenerator` interface, not on the concrete `RandomNumberGenerator` class, demonstrating dependency on abstractions rather than concrete implementations.
